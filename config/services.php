<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'weather' => [
        'enabled' => env('WEATHER_ENABLED', true),
    ],
    'public-transport' => [
        'enabled' => env('PUBLIC_TRANSPORT_ENABLED', false),
    ],
    'xite' => [
        'enabled' => env('XITE_ENABLED', false),
        'url' => env('XITE_URL', ''),
    ],

    'openweathermap' => [
        'app_id' => env('OPENWEATHERMAP_APP_ID'),
        'lat' => env('OPENWEATHERMAP_LAT', '51.9862652770478'),
        'lon' => env('OPENWEATHERMAP_LON', '5.930922684773425'),
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
