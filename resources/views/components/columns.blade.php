<div class="flex-grow min-h-0 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
    {{ $slot }}
</div>
