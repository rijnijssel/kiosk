<x-card wire:poll.visible.15s="">
    <x-card-title class="mb-0">@lang('Nieuws')</x-card-title>
    <p class="mb-3 text-gray-500 text-sm">
        @lang('bijgewerkt om :time', ['time' => now()->isoFormat('HH:mm')])
    </p>
    <ul class="flex flex-col gap-2">
        @foreach($items as $item)
            <li class="border-b pb-2 border-gray-400 grid grid-cols-4 gap-2">
                <div class="col-span-1">
                    @if($img = $item->get_enclosure()->get_link())
                        <img
                            class="w-full object-cover object-center aspect-square rounded"
                            src="{{ $img }}"
                            alt="Thumbnail for {{ $item->get_title() }}"
                        >
                    @endif
                </div>
                <div class="col-span-3 flex flex-col justify-between">
                    <h3>
                        <a href="{{ $item->get_link() }}" target="_blank">{{ $item->get_title() }}</a>
                    </h3>
                    <p class="flex flex-row justify-between text-gray-500 text-sm">
                        <span>{{ $item->get_feed()->get_title() }}</span>
                        <span>{{ Date::createFromTimestamp($item->get_date('U'))->isoFormat('dddd D MMMM YYYY, HH:mm') }}</span>
                    </p>
                </div>
            </li>
        @endforeach
    </ul>
</x-card>
