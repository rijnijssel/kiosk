<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class WeatherService
{
    /**
     * Get the weather data for a certain latitude and longitude.
     *
     * @return array<string, mixed>
     */
    public function get(): array
    {
        $lang = config('app.locale');
        $token = config('services.openweathermap.app_id');
        $lat = config('services.openweathermap.lat');
        $lon = config('services.openweathermap.lon');

        return Http::get('https://api.openweathermap.org/data/2.5/weather', [
            'appid' => $token,
            'lat' => $lat,
            'lon' => $lon,
            'mode' => 'json',
            'units' => 'metric',
            'lang' => $lang,
        ])->json();
    }
}
