<?php

namespace App\Http\Livewire\Widget;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class Calendar extends Component
{
    public function render(): View
    {
        return view('livewire.widget.calendar', [
            'agenda' => [
                'Studiedag' => '7 juni 2022',
                'Techniekdag' => '22 januari 2023, 16:00',
            ],
        ]);
    }
}
