<?php

namespace App\Http\Livewire\Widget;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class PublicTransport extends Component
{
    public function render(): View
    {
        return view('livewire.widget.public-transport', [
            'transport' => [
                'Bushalte Raapopseweg' => [
                    'Bus 9' => '5 min',
                    'Bus 331' => '12 min',
                    'Bus 7' => '18 min',
                ],
                'Station Velperpoort' => [
                    'Sprinter richting Zutphen' => '8 min',
                    'Sprinter richting Velp' => '17 min',
                ],
            ],
        ]);
    }
}
